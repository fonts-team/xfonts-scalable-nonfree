Package: xfonts-scalable-nonfree
Obtained from: XFree86 CVS repository (anoncvs@anoncvs.xfree86.org:/cvs)
Upstream author(s): The XFree86 Project, Inc., et al.

Copyright and license info:
===========================

Luxi fonts:
-----------

Luxi fonts copyright (c) 2001 by Bigelow & Holmes Inc. Luxi font 
instruction code copyright (c) 2001 by URW++ GmbH. All Rights 
Reserved. Luxi is a registered trademark of Bigelow & Holmes Inc.

Permission is hereby granted, free of charge, to any person obtaining 
a copy of these Fonts and associated documentation files (the "Font 
Software"), to deal in the Font Software, including without 
limitation the rights to use, copy, merge, publish, distribute, 
sublicense, and/or sell copies of the Font Software, and to permit 
persons to whom the Font Software is furnished to do so, subject to 
the following conditions:

The above copyright and trademark notices and this permission notice 
shall be included in all copies of one or more of the Font Software.

The Font Software may not be modified, altered, or added to, and in 
particular the designs of glyphs or characters in the Fonts may not 
be modified nor may additional glyphs or characters be added to the 
Fonts. This License becomes null and void when the Fonts or Font 
Software have been modified.

THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF 
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT 
OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT.  IN NO EVENT SHALL 
BIGELOW & HOLMES INC. OR URW++ GMBH. BE LIABLE FOR ANY CLAIM, DAMAGES 
OR OTHER LIABILITY, INCLUDING ANY GENERAL, SPECIAL, INDIRECT, 
INCIDENTAL, OR CONSEQUENTIAL DAMAGES, WHETHER IN AN ACTION OF 
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF THE USE OR 
INABILITY TO USE THE FONT SOFTWARE OR FROM OTHER DEALINGS IN THE FONT 
SOFTWARE.

Except as contained in this notice, the names of Bigelow & Holmes 
Inc. and URW++ GmbH. shall not be used in advertising or otherwise to 
promote the sale, use or other dealings in this Font Software without 
prior written authorization from Bigelow & Holmes Inc. and URW++ GmbH.

For further information, contact:

info@urwpp.de
or
design@bigelowandholmes.com

IBM Courier:
------------

********************************************************************************

IBM Courier - Copyright (c) IBM Corporation 1990, 1991

You are hereby granted permission under the terms of the IBM/MIT X Consortium
Courier Typefont agreement to execute, reproduce, distribute, display, market,
sell and otherwise transfer copies of the IBM Courier font to third parties.

The font is provided "AS IS" without charge.  NO WARRANTIES OR INDEMNIFICATION
ARE GIVEN, WHETHER EXPRESS OR IMPLIED INCLUDING, BUT LIMITED TO THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

Meltho fonts:
-------------

This license is derived from the Bigelow & Holmes Font License
found at http://www.xfree86.org/current/LICENSE2.html

The following reasonable modifications have been made to the
Bigelow & Holmes Font License:

* All references to Luxi have been replaced with Meltho

* All references to Bigelow & Holmes Inc. and URW++ GmbH haven
  been replaced with Beth Mardutho: The Syriac Institute
  
* The registered trademark notice belonging to Bigelow & Holmes
  Inc. has been removed

* Contact information has been modified to show that the
  contact person for these fonts is the Director of the
  Institute, George Kiraz.

-------------------------------------------------------------

Meltho fonts and font code copyright © 2002 by Beth Mardutho: 
The Syriac Institute. All Right Reserved.

Permission is hereby granted, free of charge, to any person 
obtaining a copy of these Fonts and associated documentation 
files (the "Font Software"), to deal in the Font Software, 
including without limitation the rights to use, copy, merge, 
publish, distribute, sublicense, and/or sell copies of the 
Font Software, and to permit persons to whom the Font Software 
is furnished to do so, subject to the following conditions:

The above copyright and trademark notices and this permission 
notice shall be included in all copies of one or more of the 
Font Software.

The Font Software may not be modified, altered, or added to, 
and in particular the designs of glyphs or characters in the 
Fonts may not be modified nor may additional glyphs or 
characters be added to the Fonts. This License becomes null 
and void when the Fonts or Font Software have been modified.

THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY 
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY 
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
AND NONINFRINGEMENT OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER 
RIGHT. IN NO EVENT SHALL BETH MARDUTHO: THE SYRIAC INSTITUTE BE 
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, INCLUDING ANY 
GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR 
FROM OTHER DEALINGS IN THE FONT SOFTWARE.

Except as contained in this notice, the name of Beth Mardutho: 
The Syriac Institute shall not be used in advertising or otherwise 
to promote the sale, use or other dealings in this Font Software 
without prior written authorization from Beth Mardutho: The 
Syriac Institute.

For further information, contact:
George Kiraz, Director of Beth Mardutho
gkiraz@bethmardutho.org
